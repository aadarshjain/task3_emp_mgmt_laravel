<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;
use App\Models\EmpRole;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    private $employeeRepository;

    // constructor for injecting repository in controller
    public function __construct(EmployeeRepositoryInterface $employeeRepository){
        $this->employeeRepository = $employeeRepository;
    }
// function for show role in add/create page while inserting new records
    public function show()
    {
        $roles=$this->employeeRepository->show_role();
        return view('registration', ['roles' => $roles]);
    }

// function for show employee that open when we go to show employee page
    public function showEmployee(Request $request){

        $filter=$request->input('filter');
        if($request->input('filter')==null){
            $filter=2;
        }
        
        $search=$request->input('search');
        if($request->input('search')==null){
            $search='';
        }

        
        $list=$this->employeeRepository->showEmployee($search,$filter);
        
        return view('showemployee', compact('list','filter','search'));
        
    }

    // function for create/add new record
    public function add(Request $request,EmployeeRequest $emp_req){
        $status = $request->input('status');
        if($status==null){
            $status=0;
        }else{
            $status=1;
        }
        
        $record=$emp_req->validated();

        $data=new Employee;
        if($files=$request->file('image')){
        $name=$files->getClientOriginalName();
        $files->move('images',$name);
        $data->path=$name;
        $data=$name;
        }

        $this->employeeRepository->store($record,$data,$request,$status);
            return back()->with('success','data inserted successfully');
    }

    // function for open edit page
    public function edit(Request $request,$id){
        // dd((int)$id);
        // dd(Employee::where('id',(int)$id)->value('id')===(int)$id);
        if(Employee::where('id',(int)$id)->value('id')===(int)$id){
        $row=Employee::where('id',$id)->first();
        
        $roles = EmpRole::pluck('role');
        $data=[
          'Info'=>$row,
          'roles' => $roles ,
          'selectedRole' => $row->role,
        ];
       return view('edit', $data);
    }else{
        return redirect()->route('showEmp')->with('record_not_found','Employee record not found');
    }
    }

    // update record function
    public function update(Request $request,EmployeeUpdateRequest $emp_req)
    {

        $update=Employee::find($request->input('empid'));
        
        $status = $request->input('status');
        if($status==null){
            $status=0;
        }else{
            $status=1;
        }
        
        $record=$emp_req->validated();
        $data=new Employee;
        if($files=$request->file('image')){
        $name=$files->getClientOriginalName();
        
        $files->move('images',$name);
        $data->path=$name;
        $data=$name;
        }else{
            $name=$update->image;
            $data->path=$name;
            $data=$name;
        }
        $this->employeeRepository->update_record($record,$data,$request,$status);
        
        return redirect()->route('showEmp')->with('update_record','Record updated successfully');
    }

    // function for update status of employee on the show employee page
    public function update_status(Request $request){
    $this->employeeRepository->update_status($request);
    return redirect()->back()->with('update_status','Status updated successfully');
    }

// function for delete particular record on the show employee page
    public function destroy(Request $request){
        $this->employeeRepository->destroy($request);
        return redirect()->back()->with('delete_status','Record deleted successfully');
    }

    // function for delete selected record on the show employee page
    public function delete_all_selected(Request $request){
        $this->employeeRepository->delete_all_selected($request);
        return response()->json(["success"=>'Employee have been deleted!']);
    }
}
