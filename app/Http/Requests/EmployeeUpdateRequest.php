<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'phone' => 'required|regex:/[0-9]{3}[0-9]{3}[0-9]{4}/|max:10',
            'skills' => 'required',
            'experience' => 'required|numeric|min:0',
            'email' => 'required|email|regex:/^[a-z0-9]+(?:[_%+.-][a-z0-9]+)?@[a-z0-9.-]+\.[a-z]{2,4}$/',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120|dimensions:max_width=100,max_height=200',
            'roles' => 'required|not_in:0'
        ];
    }

    public function messages()
    {
        return [

            'phone.max' => 'The phone number not be greater than 10 digit.',
            'phone.regex' => 'Invalid phone number.',
            'experience.min'=>'Experience must be greater than 0.',
            'experience.numeric'=>'Experience must be number.',
            'name.regex'=>'Name must be contain characters only.',
            'im age.max'=>'The image is not be greater than 5MB.',
            'image.image'=>'Upload image only.',
            'roles.not_in'=>'Please select role.',
            'image.dimensions'=>'please select image with less than or equal 100*200 resolution/dimentions'
        ];
    }
}
