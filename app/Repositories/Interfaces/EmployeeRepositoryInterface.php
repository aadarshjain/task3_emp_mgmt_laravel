<?php
namespace App\Repositories\Interfaces;
use App\Http\Requests\EmployeeRequest;

Interface EmployeeRepositoryInterface{

  
  public function show_role();
  public function showEmployee($search,$filter);
  public function store($record,$data,$request,$status);
  public function update_record($record,$data,$request,$status);
  public function update_status($request);
  public function destroy($request);
  public function delete_all_selected($request);
}