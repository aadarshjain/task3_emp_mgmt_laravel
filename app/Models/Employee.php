<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    public $directory="./images/";
    public $table='employee_management.employee';

    protected $fillable=['name','phone_no','skills','role','experience','email','image'];
    public function getPathAttribute($value)
    {
        return $this->directory.$value;
    }
    public $timestamps = false;
}
