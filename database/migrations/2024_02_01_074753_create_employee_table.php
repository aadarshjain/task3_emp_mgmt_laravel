<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_management.employee', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('phone_no');
            $table->string('skills');
            $table->string('role');
            $table->string('experience');
            $table->string('email');
   
           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_management.employee');
    }
};
