<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
          // $roles = ['Developer', 'Tester', 'Team Lead', 'Manager', 'Senior Developer'];
          $roles = DB::table('employee_management.emp_role')->pluck('role')->toArray();
          $count = DB::table('employee_management.employee')->count();
          $data = [];
  
          for ($i = $count; $i <= ($count+5); $i++) {
              $data[] = [
                  'name' => 'Employee ' . $i,
                  'phone_no' => mt_rand(1000000000, 9999999999),
                  'skills' => 'Skill ' . $i,
                  'role' => $roles[array_rand($roles)],
                  'experience' => mt_rand(1, 10),
                  'email' => 'employee' . $i . '@gmail.com',
                  'status' => mt_rand(1, 10),
              ];
          }
  
          
          DB::table('employee_management.employee')->insert($data);
       

    }
}
