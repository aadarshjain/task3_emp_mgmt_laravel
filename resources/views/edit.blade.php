@extends('master')
@section('content')

    <h1 class="text-center text-info">Edit Employee Information</h1>

{{-- Cancel button modal --}}

<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update status</h5>
          </div>
          <div class="modal-body">
              <p>Are you really want to not update the records?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
              <a href="/showEmp" type="button" class="btn btn-primary" id="">Yes</a>
          </div>
      </div>
  </div>
</div>

{{-- Cancel button modal --}}

    @if(Session::get('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
    <div class="container">

      {{-- <div class="">
        
          <button  class="btn btn-danger float-right m-2" id="btn_cancel" >Cancel</button>
      <br>
      </div> --}}

    <form action="{{route('update')}}" method="post" enctype="multipart/form-data">
        @csrf 
        <input type="hidden" name="empid" value="{{$Info->id}}">
      
        
        <div class="form-group">
        <label for="name" class="font-weight-bold">Name</label>
         <input type="text" name="name" id="name" value="{{$Info->name}}" class="form-control">
         <span style="color: red">@error('name') {{$message}} @enderror</span>
        </div><br>
        <div class="form-group">
        <label for="phone" class="font-weight-bold">Phone no.</label> 
        <input type="tel" id="phone" name="phone"  class="form-control"  value="{{$Info->phone_no}}"  />
        <span style="color: red">@error('phone') {{$message}} @enderror</span>
        </div>
        <br>
        <div class="form-group">
        <label for="skills" class="font-weight-bold">Skills</label>

        <textarea id="skills" name="skills" rows="3" cols="20" class="form-control" >{{{old('Info',$Info->skills)}}}
        </textarea>
        <span style="color: red">@error('skills') {{$message}} @enderror</span>
        </div>
        <br>
        <div class="form-group">
        <label for="email" class="font-weight-bold">Enter your email:</label>
        <input type="email" id="email" name="email" class="form-control"  value="{{$Info->email}}">
        <span style="color: red">@error('email') {{$message}} @enderror</span>
        </div>
        <br>
      
        <div class="form-group">
          <label for="roles" class="font-weight-bold">Select Role</label>
          <select name="roles" id="roles" class="form-control">
              @foreach($roles as $role)
                  <option value="{{ $role }}" @if($role == $selectedRole) selected @endif>{{ $role }}</option>
              @endforeach
          </select>
      </div>

      <div class="form-group">
        <label for="status" class="font-weight-bold">Status</label>
        <label class="switch">
            <input type="checkbox"  id="status" name="status"  @if($Info->status == 1) checked @endif onclick="$(this).attr('value', this.checked ? 1 : 0)" >
            <span class="slider round"></span>
          </label>
      </div>

       <br>
       <div class="form-group">
        <label for="experience" class="font-weight-bold">Experience(in years)</label>
        <input type="number" name="experience" id="experience"  value="{{$Info->experience}}"  class="form-control"><br>
        <span style="color: red">@error('experience') {{$message}} @enderror</span>
       </div>

       <label for="image" class="font-weight-bold">Image </label>
       <img id="output" src="/images/{{$Info->image}}" alt="current image">

       <div class="form-group">
        
        {{-- <img id="output" src="/images/{{$Info->image}}" alt="current image"> --}}
        {{-- <input type="file" name="image" id="image" class="form-control-file" src="/var/www/html/LaravelCodes/employee_management/public/images/{{$Info->image}}" value="/var/www/html/LaravelCodes/employee_management/public/images/{{$Info->image}}" ><br> --}}
        <input type="file" class="form-control" name="image" id="image" onchange="loadFile(event)">
          <span style="color: red">
                     @error('image')
                         {{ $message }}
                     @enderror
                 </span>
                 </div>
 
       <input type="submit" id="btn_update" value="Update" class="btn btn-primary">
       <button type="button" class="btn btn-danger m-2" id="btn_cancel" >Cancel</button>
      </form>
     
      </div>
    

    
@endsection

@section('scripts')
<script>

    
var loadFile=function(event){
        // var output=document.getElementById('output');
        var output = $('#output')[0];
        output.src=URL.createObjectURL(event.target.files[0]);
    } 
  $(document).ready(function(){


  

    $('#btn_cancel').click(function(e){
    $('#cancelModal').modal('show');
   });
  });
// var cancelButton = document.getElementById("btn_cancel");
// console.log(cancelButton);
// var cancelModal = document.getElementById("cancelModal");
// console.log(cancelModal);
// Add event listener to the button
// cancelButton.onclick = function() {
//     console.log('work');
    // cancelModal.classList.add("visible");
    // cancelModal.show();
    // $('#cancelModal').modal('show');
    // cancelModal.style.display = "block";
// }
  </script>
@endsection

