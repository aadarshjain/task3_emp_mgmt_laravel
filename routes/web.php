<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// route for render add page on the browser
Route::get('/',[EmployeeController::class,'show']);
// route for send data to the server
Route::post('add', [EmployeeController::class,'add']);
// route for show employee
Route::get('showEmp/{search?}/{filter?}', [EmployeeController::class,'showEmployee'])->name('showEmp');
// route for edit employee details
Route::get('edit/{id}', [EmployeeController::class,'edit']);
// route for send update record of employee
Route::post('update', [EmployeeController::class,'update'])->name('update');
// route for delete specific record on show employee page
Route::delete('delete_emp', [EmployeeController::class,'destroy']);
// route for update status specific record on show employee page
Route::put('update_status_emp', [EmployeeController::class,'update_status']);

// route for delete all selected record on show employee page
Route::delete('/selected-employee',[EmployeeController::class,'delete_all_selected'])->name('delete_selected_employee');
